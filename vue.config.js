/*
 * @Author: your name
 * @Date: 2021-09-07 18:26:20
 * @LastEditTime: 2021-09-08 23:10:49
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \low-code-visualization-form\vue.config.js
 */
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin')
module.exports = {
  chainWebpack: (config) => {
    config.plugin('loadshReplace').use(new LodashModuleReplacementPlugin())
  },
  devServer: {
    open: true,
    host: 'localhost',
    port: '9000',
    disableHostCheck: true, // 忽略热更新检查，从而不会出现热更新失败的情况
  },
}
