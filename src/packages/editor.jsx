import { computed, defineComponent, inject } from 'vue'
import { forEach as _forEach } from 'lodash'
import './editor.scss'
import EditorBlock from './editor-block'
export default defineComponent({
  props: {
    modelValue: {
      type: Object,
    },
  },
  setup(props) {
    console.log(_forEach)
    const data = computed({
      get() {
        return props.modelValue
      },
    })
    const containerStyle = computed(() => ({
      width: `${data.value.containter.width}px`,
      height: `${data.value.containter.height}px`,
      position: `${data.value.containter.position}`,
    }))
    const config = inject('config')
    return () => (
      <div class="editor">
        <div class="editor-left">
          {config.componentList.map((component) => (
            <div class="editor-left-item">
              <span>{component.label}</span>
              {component.preview()}
            </div>
          ))}
        </div>
        <div class="editor-top">menu</div>
        <div class="editor-right">attr area</div>
        <div class="editor-container">
          {/* 产生滚动条 */}
          <div class="editor-container-canvas">
            {/* 产生内容区域 */}
            <div
              class="editor-container-canvas-content"
              style={containerStyle.value}
            >
              {data.value.blocks.map((block) => (
                <EditorBlock block={block}></EditorBlock>
              ))}
            </div>
          </div>
        </div>
      </div>
    )
  },
})
