import { defineComponent, computed, inject } from 'vue'

export default defineComponent({
  props: {
    block: {
      type: Object,
    },
  },
  setup(props) {
    console.log(props)
    const containerStyle = computed(() => ({
      position: `${props.block.position}`,
      zIndex: `${props.block.zIndex}`,
      width: `${props.block.width}px`,
      height: `${props.block.height}px`,
      top: `${props.block.top}px`,
      left: `${props.block.left}px`,
    }))
    const config = inject('config')
    console.log(config, 'inject config')
    return () => {
      const componet = config.componentMap[props.block.key]
      const renderComponet = componet.render()
      return <div style={containerStyle.value}>{renderComponet}</div>
    }
  },
})
