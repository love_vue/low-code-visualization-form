/*
 * @Author: your name
 * @Date: 2021-09-06 21:31:28
 * @LastEditTime: 2021-09-08 23:35:41
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \low-code-visualization-form\src\main.js
 */
import { createApp } from 'vue'
import App from './App.vue'

import 'element-plus/theme-chalk/index.css'
createApp(App).mount('#app')
